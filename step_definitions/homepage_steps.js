const { I } = inject();
const assert = require('assert');
const homepage = require ('../pages/homepage.js');

Given('I am on the Oranum homepage', () => {
  I.amOnPage(homepage.url);
 
});

When('I search for {string}',(searchText) => {

  homepage.searchForText(searchText);
});

Then('all results should contain {string}', async (expectedText) => {
const results= await homepage.getSearchResults();
results.forEach((result) =>
 {
  assert.ok(result.toLowerCase().includes(expectedText.toLowerCase()));
});

});
