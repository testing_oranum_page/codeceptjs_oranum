

const {I} = inject();
module.exports = {

url:'https://oranum.com',

searchInput:"input[placeholder='Search for Expert or category']",
searchResults:"h1[class='listpage-title']",
OutputResult:".thumb-data-item--name",
ClickResult:".toolbar-search-button",

// CodeceptJS before hook

searchForText(text){
  I.fillField(this.searchInput,text);
  I.wait(4);
  I.click(this.ClickResult);
  
  
},

getSearchResults()

{
  I.wait(4);
  const results = I.grabTextFromAll(this.OutputResult);
  return results;
  
},


  
};



