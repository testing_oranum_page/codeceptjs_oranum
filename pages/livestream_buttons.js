const { I } = inject();
const assert= require('assert');
const psychicLivestreamUrl = 'https://oranum.com/en/chat/LovePsychyicAnie';

module.exports = {
  
  liveIndicator: "[data-testid='badgeLive']",
  getCreditsButton: "//div[@data-id='buyCreditIcon']",
  addToFavoritesButton: ".mc_heart_anim_container",
  surpriseButton: "div[data-testid='surprise-OranumSurprisesGlobe_LJ']",
  startSessionButton:"Start Private Session",
  getCoinsButton: "//div[@class='mc_bottom_action_bar']",
  closebutton:"//a[@class='mc_dialog__close js_close_dialog'][1]",
  signupDialogue: ".mc_dialog__header",
  getFirstData : "[class='thumb-data-item--name']",

  async isPsychicLive() {

    const firstData = locate(this.getFirstData).first();
    I.wait(3);
    await I.click(firstData);
    I.wait(2);
    await I.waitForElement(this.liveIndicator, 40);

   console.log('Checking if the live indicator element is visible');
    return await I.seeElement(this.liveIndicator);
  },

  async clickButton(button) {
    
    try {
      switch (button) {
        case 'Get Credits':
          await I.click(this.getCreditsButton);
          break;
        case 'Add to Favorites':
          await I.click(this.addToFavoritesButton);
          break;
        case 'Surprise':
          await I.click(this.surpriseButton);
          break;
        case 'Start Session':
          await I.click(this.startSessionButton);
          break;
        case 'Get Coins':
          await I.click(this.getCoinsButton);
          break;
        default:
          throw new Error(`Button ${button} not recognized`);
      }
    } catch (error) {
      console.error("Error clicking button:", error);
    }
  },

  async signUpDisplayed() {
    try {
      await I.waitForElement(this.signupDialogue, 60, { visible: true , interval:1000 });
      const signUpOverlay = await I.grabTextFrom(this.signupDialogue);
      const targetText = "Sign up";
      const signUpOverlayLowerCase = signUpOverlay.toLowerCase();
      const targetTextLowerCase = targetText.toLowerCase();
      const message = signUpOverlayLowerCase.includes(targetTextLowerCase)
       ? `The signUpOverlay contains the text "${targetText}" (case-insensitive).`
       : `The signUpOverlay does not contain the text "${targetText}" (case-insensitive).`;

     assert(signUpOverlayLowerCase.includes(targetTextLowerCase), message, 4);
       await I.waitForElement(this.closebutton, 5);
      const firstElement = locate(this.closebutton).first();
      I.wait(3);
      await I.click(firstElement);
      I.wait(2);
      console.log("First element is clicked successfully");
         } catch (error) {
      console.error("Error:", error);
    }
  }
};
