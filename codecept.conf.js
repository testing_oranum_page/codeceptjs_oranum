let closeBrowser = true;
//const baseUrl='https://www.oranum.com';
exports.config = {
  output: './output',
  helpers: {
    Playwright: {
      show: true,
      browser: 'chromium',
      restart:'session',
      waitForTimeout:15000,
      getPageTimeout: 60000,
    },
  },
  include: {
    I: './steps_file.js',
    homepagePage: './pages/homepage.js',
    livestreamButtons:'./pages/livestream_buttons.js',
    ValidatehomePage:'./pages/ValidatePage.js'
  },
  mocha: {},
  bootstrap: null,
  timeout: null,
  teardown: null,
  gherkin: {
    features: './features/*.feature',
    steps: ['./step_definitions/homepage_steps.js', './step_definitions/livestream_buttons_steps.js' , './step_definitions/homepagevalidation_steps.js']
  },
  plugins: {
    allure: {
      enabled: true,
      outputDir: './output/allure-results',
      require: '@codeceptjs/allure-legacy',

    },
    screenshotOnFail: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    retryFailedStep: {
      enabled: false
    },
    retryTo: {
      enabled: true
    },
    eachElement: {
      enabled: true
    },
    pauseOnFail: {}
  },
  stepTimeout: 0,
  stepTimeoutOverride: [{
      pattern: 'wait.*',
      timeout: 0
    },
    {
      pattern: 'amOnPage',
      timeout: 0
    }
  ],

  tests: './*_test.js',
  name: 'Test_Project',

  

};