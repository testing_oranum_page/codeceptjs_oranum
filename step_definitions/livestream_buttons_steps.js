const { I } = inject();
const livestreamButtons = require('../pages/livestream_buttons');

Given('I am on the psychic livestream page {string}', async (psychicUrl) => {
 // I.amOnPage(psychicUrl);
});

Given('the psychic is live', async() => {
    await livestreamButtons.isPsychicLive();
});

When('I click the {string} button', async (button) => {
  await livestreamButtons.clickButton(button);
});

Then('the {string} overlay is displayed', async (overlay) => {
 
    await livestreamButtons.signUpDisplayed();

 
});
