Feature: Search Psychics
  
Scenario Outline: Search for different terms
   Given I am on the Oranum homepage
    When I search for '<searchTerm>'
    Then all results should contain '<expectedTerm>'

  Examples:
    | searchTerm | expectedTerm |
    | Matt       | matt         |
    | Myst       | myst         |
    | Ann        | ann           |
    |pys         | pys          |
   


 