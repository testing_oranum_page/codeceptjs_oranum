Feature: Validate 'sign up' overlay triggers for buttons on psychic livestream

Scenario: Check the psychic is live and validate the buttons
  
  Given I am on the Oranum homepage 
  And the psychic is live

  #When I click the 'Start Session' button
  #Then the 'Sign up' overlay is displayed

  When I click the 'Get Credits' button
  Then the 'Sign up' overlay is displayed

  When I click the 'Add to Favorites' button
  Then the 'Sign up' overlay is displayed

  
  When I click the 'Surprise' button
  Then the 'Sign up' overlay is displayed

  When I click the 'Get Coins' button
  Then the 'Sign up' overlay is displayed

  

 
  
