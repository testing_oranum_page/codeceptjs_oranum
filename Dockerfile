# Use an official Node.js runtime as a parent image
FROM mcr.microsoft.com/playwright:focal

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install dependencies
RUN npm install
RUN npm install playwright@latest

# Copy the rest of the application code to the container
COPY . .

# Expose the port on which CodeceptJS will run (if necessary)
EXPOSE 1337

# Run CodeceptJS when the container launches
CMD ["npx", "codeceptjs", "run"]
