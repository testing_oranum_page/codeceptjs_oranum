const { I } = inject();
const assert= require('assert');

class ValidatePage {

  async navigateToOranumHomePage() {
    I.amOnPage('https://www.oranum.com');
  }

  async clickTopic(topic) {
   
    I.click(topic);
    I.wait(5);
  }

  async verifyElementsNotRepeated() {
    console.log("Checking for repeated elements...");
    const elements =await I.grabTextFromAll(".thumb-data-item--name");
    console.log("All elements:", elements);
    const uniqueElements = Array.from(new Set(elements));
    assert.strictEqual(uniqueElements.length, elements.length, 'Duplicate elements found.');
    console.log('All elements are unique.');
 }
  async verifyMatchingContent(topic) {
    I.seeElement(`//a[@class='sidebar-filters-link' and contains(text(), '${topic}')]`);
  }
}

module.exports = new ValidatePage();
