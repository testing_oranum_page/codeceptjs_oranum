const{I}=inject();
const ValidatePage = require('../pages/ValidatePage.js');

Given('I am on the Oranum page', () => {

    ValidatePage.navigateToOranumHomePage();
});

When('I click on the {string} link',(topic)=> {
 
  ValidatePage.clickTopic(topic);
 
});
Then('I should see only matching content for {string}', async(topic) => {
  //console.log("hi");
  //const elementSelector="a.sidebar-filters-link";
  //await ValidatePage.verifyMatchingContent(elementSelector,topic);
  //console.log("validating");
  await ValidatePage.verifyElementsNotRepeated();
});

 