Instructions to run the playwright test locally

Step 1: Clone the Repository
Open your terminal or command prompt and clone the GitLab repository to your local machine:
git clone https://gitlab.com/testing_oranum_page/codeceptjs_oranum.git

Step 2: Navigate to the Project Directory
Change your working directory to the cloned project:
cd codeceptjs_oranum

Step 3: Install Node.js and npm
Ensure that you have Node.js and npm installed on your machine. You can download them from https://nodejs.org/. This will also install npm, the Node.js package manager.

Step 4: Install Dependencies
Run the following command to install the project dependencies (including CodeceptJS and Playwright):
npm install

Step 5: Run the tests 
Now you can run the tests using the following command:

npm run test- This command will run all the scenarios.
npm run scenario1- This command will run all the scenarios1.
npm run scenario2-This command will run all the scenarios2.
npm run scenario3-This command will run all the scenarios3.

Step 4: Instruction to generate the Allure reports
Note: The scripts in package.json are configured to run the test with Allure.
To generate the Allure report run the below command after running the test
allure generate ./output/allure-results --clean -o ./output/allure-report

To open the generated Allure report run the below command
allure open ./output/allure-report



Instructions to run the playwright test with Docker(Make file)
Prequisite: Docker must be installed.
Run the below commands:
make build
make run
