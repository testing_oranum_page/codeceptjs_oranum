Feature: Oranum Homepage Validation

Scenario Outline: Validate different topics on the Oranum homepage
  Given I am on the Oranum page
  When I click on the '<topic>' link
  Then I should see only matching content for '<topic>'

Examples:
  | topic               |
  | Astrology           |
  | Tarot               |
  | Clairvoyance        |
  | Dream interpretation|
  | Healing             |
  | Crystals            |
  | Mediumship          |
  | Crystal ball        |
  | Numerology          |
  | Runes               |
  | Palm reading        |
  | Energy work         |
